let collection = [];

// Write the queue functions below.

function print() {
  return collection;
}


function enqueue(name) {
  collection.push(name);
  return collection;
}


function dequeue() {
  collection.shift();
  return collection;
}


function front() {
  return collection[0];
}


function size() {
  return collection.length;
}

function isEmpty() {
  if (collection.length == 0) {
    return true;
  } else {
    return false;
  }
}

module.exports = {
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty,
};